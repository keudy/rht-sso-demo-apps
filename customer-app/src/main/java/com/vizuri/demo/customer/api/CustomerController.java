package com.vizuri.demo.customer.api;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.IDToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vizuri.demo.customer.service.CustomerService;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Customer portal controller.
 */
@Controller
//@CacheControl(policy = CachePolicy.NO_CACHE)
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @NotNull
    @Value("${product.service.url}")
    private String productServiceUrl;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String handleHomePageRequest(Model model) {
        return "home";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String handleCustomersRequest(Principal principal, Model model) {
        model.addAttribute("customers", customerService.getCustomers());
        model.addAttribute("principal",  principal);
        
        if (principal instanceof KeycloakPrincipal) {
    			System.out.println(">>> Is a  a KeycloakPricipal");

            KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
            IDToken token = kp.getKeycloakSecurityContext().getIdToken();

            Map<String, Object> otherClaims = token.getOtherClaims();
            Set<Entry<String,Object>> es = otherClaims.entrySet();
            for (Entry<String, Object> entry : es) {
				System.out.println(">>> Entry:" + entry.getKey() + ":" + entry.getValue());
			}

        }
        else {
        		System.out.println(">>> Not a KeycloakPricipal");
        }
        
        return "customers";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String handleAdminRequest(Principal principal, Model model) {
        model.addAttribute("principal",  principal);
        return "admin";
    }

    @ModelAttribute("productServiceUrl")
    public String populateProductServiceUrl() {
        return productServiceUrl;
    }

    @ModelAttribute("serviceName")
    public String populateServiceName() {
        return "Customer Portal";
    }
}
