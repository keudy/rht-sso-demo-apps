rhsso_url=https://secure-sso-rht-sso.192.168.99.100.nip.io

TKN=$(curl -v -X POST "$rhsso_url/auth/realms/demo-realm/protocol/openid-connect/token" \
 -H "Content-Type: application/x-www-form-urlencoded" \
 -d "username=testuser" \
 -d "password=password" \
 -d "grant_type=password" \
 -d "client_id=product-app" \
  --cacert $HOME/ssl_ca/acme-ca.crt \
| sed 's/.*access_token":"//g' | sed 's/".*//g')

echo "TOKEN: $TKN"

curl -v -X GET \
       -H "Authorization: Bearer $TKN" \
       http://localhost:8081/products 

#curl -v -X GET \
       #-H "Accept: application/json" \
       #-H "Authorization: Bearer $TKN" \
       #http://`oc get route/wf-swarm-oauth -n $OCP_PROJECT_PREFIX-bservices --template "{{.spec.host}}"`/time/now
