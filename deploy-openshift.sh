SERVICE_GIT_URL=https://keudy@bitbucket.org/keudy/rht-sso-demo-apps.git
PROJECT=sso-apps

oc new-project $PROJECT

oc create secret generic ssl-truststore --from-file=$HOME/ssl_ca/truststore.jks

oc new-app --file=openshift/templates/docker-container-pipeline.yaml -p APP_NAME=springboot-base-container -p GIT_SOURCE_URL=${SERVICE_GIT_URL} -p GIT_SOURCE_REF=master -p CONTEXT_DIR=springboot-base-container

oc new-app --file=openshift/templates/springboot-pipeline.yaml -p APP_NAME=database-service -p GIT_SOURCE_URL=${SERVICE_GIT_URL} -p GIT_SOURCE_REF=master -p CONTEXT_DIR=database-service -p JAVA_OPTS="-Djavax.net.ssl.trustStore=/etc/ssl-truststore/truststore.jks -Djavax.net.ssl.trustStorePassword=P@ssw0rd  -Xms128m -Xmx256m" -p KEYCLOAK_URL=https://secure-sso-rht-sso.192.168.99.100.nip.io/auth -p KEYCLOAK_REALM=spring-demo -p KEYCLOAK_RESOURCE=database-service

oc new-app --file=openshift/templates/springboot-pipeline.yaml -p APP_NAME=product-app -p GIT_SOURCE_URL=${SERVICE_GIT_URL} -p GIT_SOURCE_REF=master -p CONTEXT_DIR=product-app -p JAVA_OPTS="-Djavax.net.ssl.trustStore=/etc/ssl-truststore/truststore.jks -Djavax.net.ssl.trustStorePassword=P@ssw0rd  -Xms128m -Xmx256m" -p KEYCLOAK_URL=https://secure-sso-rht-sso.192.168.99.100.nip.io/auth -p KEYCLOAK_REALM=spring-demo -p KEYCLOAK_RESOURCE=product-protal

oc new-app --file=openshift/templates/springboot-pipeline.yaml -p APP_NAME=customer-app -p GIT_SOURCE_URL=${SERVICE_GIT_URL} -p GIT_SOURCE_REF=master -p CONTEXT_DIR=customer-app -p JAVA_OPTS="-Djavax.net.ssl.trustStore=/etc/ssl-truststore/truststore.jks -Djavax.net.ssl.trustStorePassword=P@ssw0rd  -Xms128m -Xmx256m" -p KEYCLOAK_URL=https://secure-sso-rht-sso.192.168.99.100.nip.io/auth -p KEYCLOAK_REALM=spring-demo -p KEYCLOAK_RESOURCE=customer-portal


oc start-build -w springboot-base-container
oc start-build database-service
oc start-build product-app
oc start-build customer-app


