rhsso_url=https://secure-sso-rht-sso.192.168.99.100.nip.io
realm=spring-demo


#TKN=$(curl -X POST "$rhsso_url/auth/realms/rht_gpte_3scale_realm/protocol/openid-connect/token" \
#TKN=$(curl -v -X POST "$rhsso_url/auth/realms/spring-demo/protocol/openid-connect/token" \
TKN=$(curl -v -X POST "$rhsso_url/auth/realms/spring-demo/protocol/openid-connect/token" \
 -H "Content-Type: application/x-www-form-urlencoded" \
 -d "username=user" \
 -d "password=password" \
 -d "grant_type=password" \
 -d "client_id=database-service" \
  --cacert $HOME/ssl_ca/acme-ca.crt \
| sed 's/.*access_token":"//g' | sed 's/".*//g')

echo $TKN

curl -v -X GET \
       -H "Accept: application/json" \
       -H "Authorization: Bearer $TKN" \
       http://localhost:8081/database/customers
       #http://`oc get route/wf-swarm-oauth -n bservices --template "{{.spec.host}}"`/time/now
